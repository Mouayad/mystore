package com.example.mystore;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;

public class Login extends AppCompatActivity {
    EditText et_un, et_pass;
    Button btn_log;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        et_un = findViewById(R.id.et_un);
        et_pass = findViewById(R.id.et_pass);
        btn_log = findViewById(R.id.btn_log);

        btn_log.setOnClickListener(v -> {
            String username = et_un.getText().toString().trim();
            String password = et_pass.getText().toString().trim();

            if(username.equals("admin") && password.equals("admin")){
                //  Useing Intent to redirect to another activity (from login layout to the main activity)
                Intent intent = new Intent(Login.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}