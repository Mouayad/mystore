package com.example.mystore;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;

@SuppressLint("UseSwitchCompatOrMaterialCode")
public class MainActivity extends AppCompatActivity {

    SQLiteDatabase StoreDB;
    Switch soundSwitch;
    AudioManager audioManager;
    TextView type, mat, agt, inv, invde;
    EditText et_TypeId, et_TypeName, et_TypeDescription, et_MaterialId, et_MaterialName;
    EditText et_MaterialTypeId, et_MaterialDescription, et_AgentId, et_AgentName;
    EditText et_AgentDescription, et_InvoiceId, et_InvoiceAgentId, et_InvoiceDate;
    EditText et_InvoiceIsBuy, et_InvoiceDescription, et_InvoiceDetailId, et_InvoiceDetailInvoiceId;
    EditText et_InvoiceDetailMaterialId, et_InvoiceDetailNumber, et_InvoiceDetailPrice;
    EditText et_invoiceDetailTotal;

    Button btn_AddType, btn_AddMaterial, btn_AddAgent, btn_AddInvoice, btn_addInvoiceDetail;

    Button Show_Details;

    Spinner themeSpinner;
    ScrollView mainLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_AddType = findViewById(R.id.btn_AddType);
        btn_AddMaterial = findViewById(R.id.btn_AddMaterial);
        btn_AddAgent = findViewById(R.id.btn_AddAgent);
        btn_AddInvoice = findViewById(R.id.btn_AddInvoice);
        btn_addInvoiceDetail = findViewById(R.id.btn_addInvoiceDetail);
        Show_Details = findViewById(R.id.showInvoiceButton);

        et_TypeId =findViewById(R.id.et_TypeId);
        et_TypeName =findViewById(R.id.et_TypeName);
        et_TypeDescription =findViewById(R.id.et_TypeDescription);
        et_MaterialId =findViewById(R.id.et_MaterialId);
        et_MaterialName =findViewById(R.id.et_MaterialName);
        et_MaterialTypeId =findViewById(R.id.et_MaterialTypeId);
        et_MaterialDescription =findViewById(R.id.et_MaterialDescription);
        et_AgentId =findViewById(R.id.et_AgentId);
        et_AgentName =findViewById(R.id.et_AgentName);
        et_AgentDescription =findViewById(R.id.et_AgentDescription);
        et_InvoiceId =findViewById(R.id.et_InvoiceId);
        et_InvoiceAgentId =findViewById(R.id.et_InvoiceAgentId);
        et_InvoiceDate =findViewById(R.id.et_InvoiceDate);
        et_InvoiceIsBuy = findViewById(R.id.et_InvoiceIsBuy);
        et_InvoiceDescription =findViewById(R.id.et_InvoiceDescription);
        et_InvoiceDetailId =findViewById(R.id.et_InvoiceDetailId);
        et_InvoiceDetailInvoiceId =findViewById(R.id.et_InvoiceDetailInvoiceId);
        et_InvoiceDetailMaterialId =findViewById(R.id.et_InvoiceDetailMaterialId);
        et_InvoiceDetailNumber =findViewById(R.id.et_InvoiceDetailNumber);
        et_InvoiceDetailPrice =findViewById(R.id.et_InvoiceDetailPrice);
        et_invoiceDetailTotal =findViewById(R.id.et_invoiceDetailTotal);
        soundSwitch = findViewById(R.id.sound_switch);

        type = findViewById(R.id.TYPE);
        mat = findViewById(R.id.MAT);
        agt = findViewById(R.id.AGT);
        inv = findViewById(R.id.INV);
        invde = findViewById(R.id.INVDE);

        themeSpinner = findViewById(R.id.theme_spinner);
        mainLayout = findViewById(R.id.main_layout);
        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        soundSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                // The switch is enabled/checked, set volume to the maximum
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                        audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
            } else {
                // The switch is disabled/unchecked, mute the volume
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
            }
        });

        themeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedTheme = parent.getItemAtPosition(position).toString();
                switch (selectedTheme) {
                    case"White":
                        mainLayout.setBackgroundColor(Color.WHITE);
                        setButtonStyle(Color.WHITE,Color.parseColor("#03A9F4"));
                        setTextViewStyle(Color.BLACK);
                        HintColor(Color.BLACK);
                        break;
                    case "Red":
                        mainLayout.setBackgroundColor(Color.RED);
                        setButtonStyle(Color.RED, Color.WHITE);
                        setTextViewStyle(Color.WHITE);
                        HintColor(Color.WHITE);
                        themeSpinner.setBackgroundColor(Color.WHITE);
                        break;
                    case "Black":
                        mainLayout.setBackgroundColor(Color.BLACK);
                        setButtonStyle(Color.BLACK, Color.WHITE);
                        setTextViewStyle(Color.WHITE);
                        HintColor(Color.WHITE);
                        themeSpinner.setBackgroundColor(Color.WHITE);
                        break;
                    case "Blue":
                        mainLayout.setBackgroundColor(Color.BLUE);
                        setButtonStyle(Color.BLUE, Color.WHITE);
                        setTextViewStyle(Color.WHITE);
                        HintColor(Color.WHITE);
                        themeSpinner.setBackgroundColor(Color.WHITE);
                        break;
                    case "Purple":
                        mainLayout.setBackgroundColor(Color.parseColor("#800080"));
                        setButtonStyle(Color.parseColor("#800080"), Color.WHITE);
                        setTextViewStyle(Color.WHITE);
                        HintColor(Color.WHITE);
                        themeSpinner.setBackgroundColor(Color.WHITE);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        StoreDB= openOrCreateDatabase("StoreDB", Context.MODE_PRIVATE,null);

        StoreDB.execSQL("CREATE TABLE IF NOT EXISTS Type (" +"id INTEGER PRIMARY KEY AUTOINCREMENT," +"name TEXT," +"description TEXT)");
        StoreDB.execSQL("CREATE TABLE IF NOT EXISTS Material (" +"id INTEGER PRIMARY KEY AUTOINCREMENT," +"name TEXT," +"Type_Id INTEGER," +"description TEXT," +"FOREIGN KEY (Type_Id) REFERENCES Type(id))");
        StoreDB.execSQL("CREATE TABLE IF NOT EXISTS agent (" +"id INTEGER PRIMARY KEY AUTOINCREMENT," +"name TEXT," +"description TEXT)");
        StoreDB.execSQL("CREATE TABLE IF NOT EXISTS Invoice (" +"id INTEGER PRIMARY KEY AUTOINCREMENT," +"agent_id INTEGER," +"date TEXT," +"isbuy INTEGER," +"description TEXT," +"FOREIGN KEY (agent_id) REFERENCES agent(id))");
        StoreDB.execSQL("CREATE TABLE IF NOT EXISTS Invoice_detail (" +"id INTEGER PRIMARY KEY AUTOINCREMENT," +"Invoice_id INTEGER," +"Material_id INTEGER," +"number INTEGER," +"price REAL," +"total REAL," +"FOREIGN KEY (Invoice_id) REFERENCES Invoice(id)," +"FOREIGN KEY (Material_id) REFERENCES Material(id))");



    }
    public void onClick(View V){
        if(V == btn_AddType){
            StoreDB.execSQL("INSERT INTO Type VALUES('" + et_TypeId.getText() + "','" + et_TypeName + "','" + et_TypeDescription + "');");
            Toast.makeText(this, "SUCCESS, Type ADDED", Toast.LENGTH_SHORT).show();
            clear();
        }
        if(V == btn_AddMaterial){
            StoreDB.execSQL("INSERT INTO Material VALUES('" + et_MaterialId.getText() + "','" + et_MaterialName.getText() + "','" + et_MaterialTypeId.getText() + "','" + et_MaterialDescription.getText() + "');");
            Toast.makeText(MainActivity.this, "Material added successfully", Toast.LENGTH_SHORT).show();
            clear();
        }
        if(V == btn_AddAgent){
            StoreDB.execSQL("INSERT INTO agent VALUES('" + et_AgentId.getText() + "','" + et_AgentName.getText() + "','" + et_AgentDescription.getText() + "');");
            Toast.makeText(MainActivity.this, "Agent added successfully", Toast.LENGTH_SHORT).show();
            clear();
        }
        if(V == btn_AddInvoice){
            StoreDB.execSQL("INSERT INTO Invoice VALUES('" + et_InvoiceId.getText() + "','" + et_InvoiceAgentId.getText() + "','" + et_InvoiceDate.getText()+ "','" + et_InvoiceIsBuy.getText() + "','" + et_InvoiceDescription.getText()  + "');");
            Toast.makeText(MainActivity.this, "Invoice added successfully", Toast.LENGTH_SHORT).show();
            clear();
        }
        if(V == btn_addInvoiceDetail){
            StoreDB.execSQL("INSERT INTO Invoice_detail VALUES('" + et_InvoiceDetailId.getText() + "','" + et_InvoiceDetailInvoiceId.getText() + "','" + et_InvoiceDetailMaterialId.getText()+ "','" + et_InvoiceDetailNumber.getText() + "','" + et_InvoiceDetailPrice.getText() + "','" + et_invoiceDetailTotal.getText() + "');");
            Toast.makeText(MainActivity.this, "Invoice_detail added successfully", Toast.LENGTH_SHORT).show();
            clear();
        }


    }
    public void clear(){
        et_TypeId.setText("");
        et_TypeName.setText("");
        et_TypeDescription.setText("");
        et_MaterialId.setText("");
        et_MaterialName.setText("");
        et_MaterialTypeId.setText("");
        et_MaterialDescription.setText("");
        et_AgentId.setText("");
        et_AgentName.setText("");
        et_AgentDescription.setText("");
        et_InvoiceId.setText("");
        et_InvoiceAgentId.setText("");
        et_InvoiceDate.setText("");
        et_InvoiceIsBuy.setText("");
        et_InvoiceDescription.setText("");
        et_InvoiceDetailId.setText("");
        et_InvoiceDetailInvoiceId.setText("");
        et_InvoiceDetailMaterialId.setText("");
        et_InvoiceDetailNumber.setText("");
        et_InvoiceDetailPrice.setText("");
        et_invoiceDetailTotal.setText("");
    }
    private void setButtonStyle(int textColor, int backgroundColor) {
        btn_AddType = findViewById(R.id.btn_AddType);
        btn_AddMaterial = findViewById(R.id.btn_AddMaterial);
        btn_AddAgent = findViewById(R.id.btn_AddAgent);
        btn_AddInvoice = findViewById(R.id.btn_AddInvoice);
        btn_addInvoiceDetail = findViewById(R.id.btn_addInvoiceDetail);
        Show_Details = findViewById(R.id.showInvoiceButton);
        // Set text color and background color for all buttons
        btn_AddType.setTextColor(textColor);
        btn_AddType.setBackgroundColor(backgroundColor);

        btn_AddMaterial.setTextColor(textColor);
        btn_AddMaterial.setBackgroundColor(backgroundColor);

        btn_AddAgent.setTextColor(textColor);
        btn_AddAgent.setBackgroundColor(backgroundColor);

        btn_AddInvoice.setTextColor(textColor);
        btn_AddInvoice.setBackgroundColor(backgroundColor);

        btn_addInvoiceDetail.setTextColor(textColor);
        btn_addInvoiceDetail.setBackgroundColor(backgroundColor);

        Show_Details.setTextColor(textColor);
        Show_Details.setBackgroundColor(backgroundColor);
        // Add more buttons as needed
    }
    private void setTextViewStyle(int textColor) {

        // Set text color for all TextViews
        type.setTextColor(textColor);
        mat.setTextColor(textColor);
        agt.setTextColor(textColor);
        inv.setTextColor(textColor);
        invde.setTextColor(textColor);

        et_TypeId.setTextColor(textColor);
        et_TypeName.setTextColor(textColor);
        et_TypeDescription.setTextColor(textColor);
        et_MaterialId.setTextColor(textColor);
        et_MaterialName.setTextColor(textColor);
        et_MaterialTypeId.setTextColor(textColor);
        et_MaterialDescription.setTextColor(textColor);
        et_AgentId.setTextColor(textColor);
        et_AgentName.setTextColor(textColor);
        et_AgentDescription.setTextColor(textColor);
        et_InvoiceId.setTextColor(textColor);
        et_InvoiceAgentId.setTextColor(textColor);
        et_InvoiceDate.setTextColor(textColor);
        et_InvoiceIsBuy.setTextColor(textColor);
        et_InvoiceDescription.setTextColor(textColor);
        et_InvoiceDetailId.setTextColor(textColor);
        et_InvoiceDetailInvoiceId.setTextColor(textColor);
        et_InvoiceDetailMaterialId.setTextColor(textColor);
        et_InvoiceDetailNumber.setTextColor(textColor);
        et_InvoiceDetailPrice.setTextColor(textColor);
        et_invoiceDetailTotal.setTextColor(textColor);

        // Add more TextViews as needed
    }
    public void HintColor(int HintColor){
        et_TypeId.setHintTextColor(HintColor);
        et_TypeName.setHintTextColor(HintColor);
        et_TypeDescription.setHintTextColor(HintColor);
        et_MaterialId.setHintTextColor(HintColor);
        et_MaterialName.setHintTextColor(HintColor);
        et_MaterialTypeId.setHintTextColor(HintColor);
        et_MaterialDescription.setHintTextColor(HintColor);
        et_AgentId.setHintTextColor(HintColor);
        et_AgentName.setHintTextColor(HintColor);
        et_AgentDescription.setHintTextColor(HintColor);
        et_InvoiceId.setHintTextColor(HintColor);
        et_InvoiceAgentId.setHintTextColor(HintColor);
        et_InvoiceDate.setHintTextColor(HintColor);
        et_InvoiceIsBuy.setHintTextColor(HintColor);
        et_InvoiceDescription.setHintTextColor(HintColor);
        et_InvoiceDetailId.setHintTextColor(HintColor);
        et_InvoiceDetailInvoiceId.setHintTextColor(HintColor);
        et_InvoiceDetailMaterialId.setHintTextColor(HintColor);
        et_InvoiceDetailNumber.setHintTextColor(HintColor);
        et_InvoiceDetailPrice.setHintTextColor(HintColor);
        et_invoiceDetailTotal.setHintTextColor(HintColor);
    }
}